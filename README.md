# Step project Forkio

Step project Forkio

### USED TECHNOLOGIES
- HTML, CSS, JS
- GULP (WEBPACK)
- JQUERY
- SASS (SCSS)
- NPM as package manager
- BEM

### TEAM MEMBERS
- Ilia Dubov
- Yurii Vovchuk

### TASKS FOR TEAM
- Ilia Dubov: 
    1. Created header and testimonials sections
    2. Worked with additional tasks
- Yurii Vovchuk: 
    1. Created editor and pricing sections
- Team: 
    1. Created repository
    2. Added gulpfile
    3. Installed all packages needed for development
    4. Added .gitignore file