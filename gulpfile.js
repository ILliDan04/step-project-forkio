// GULP MODULE
const gulp = require("gulp");

// LIVE SERVER
const browserSync = require("browser-sync").create();

// CSS
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const prefix = require("gulp-autoprefixer");

// MINIFICATION
const cleanCss = require("gulp-clean-css");
const purgeCss = require("gulp-purgecss");
const jsmin = require("gulp-jsmin");
const imagemin = require("gulp-imagemin");

// DIR CONTROL
const clean = require("gulp-clean");
const rename = require("gulp-rename");

// GULP ERROR HANDLING
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");

// TASK FUNCTIONS
function css() {
    return gulp.src("./src/scss/*").
        pipe(plumber({
            errorHandler: notify.onError(function(err) {
                browserSync.notify(err.message, 6000);
                return {
                    title: "Css Error!",
                    message: err.message,
                }
        })})).
        pipe(sass()).
        pipe(concat("main.css")).
        pipe(prefix()).
        pipe(cleanCss()).
        pipe(purgeCss({
            content: ["./index.html"],
        })).
        pipe(rename({suffix: ".min"})).
        pipe(gulp.dest("./dist/css")).
        pipe(browserSync.stream());
}

function js() {
    return gulp.src(["./src/js/*", "!./src/js/app.js"]).
        pipe(plumber({
            errorHandler: notify.onError(function(err) {
                browserSync.notify(err.message, 6000);
                return {
                    title: "Js Error!",
                    message: err.message,
                }
        })})).
        pipe(concat("main.js")).
        pipe(jsmin()).
        pipe(rename({suffix: ".min"})).
        pipe(gulp.dest("./dist/js")).
        pipe(browserSync.stream());
}

function images() {
    return gulp.src("./src/img/*").
        pipe(plumber({
            errorHandler: notify.onError(function(err) {
                browserSync.notify(err.message, 6000);
                return {
                    title: "Images Error!",
                    message: err.message,
                }
        })})).
        pipe(imagemin()).
        pipe(gulp.dest("./dist/img")).
        pipe(browserSync.stream());
}

function clear() {
    return gulp.src("./dist").
        pipe(plumber({
            errorHandler: notify.onError(function(err) {
                browserSync.notify(err.message, 6000);
                return {
                    title: "Clear Error!",
                    message: err.message,
                }
        })})).
        pipe(clean());
}

function server() {
    browserSync.init({
        server: {
            baseDir: "./",
        }
    });

    gulp.watch("./*.html").on("change", browserSync.reload);
}

function watcher() {
    gulp.watch("./src/scss/*", css);
    gulp.watch("./src/js/*", js);
    gulp.watch("./src/img", images);
}

// TASKS
gulp.task("build", gulp.series(clear, css, js));
gulp.task("dev", gulp.series(css, js, images, gulp.parallel(watcher, server)));