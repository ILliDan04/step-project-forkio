const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");

module.exports = {
    entry: "./src/js/app.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        publicPath: "/dist",
        filename: "js/main.min.js",
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "img/[name].[ext]",
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                loader: "raw-loader",
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "../",
                        }
                    },
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            plugins: [
                                autoprefixer(),
                                cssnano(),
                            ]
                        }
                    },
                    "group-css-media-queries-loader",
                    "sass-loader",
                ]
            },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "css/main.min.css",
        }),
    ],
    devServer: {
        contentBase: "./",
        port: 9000,
        overlay: true,
    },
}