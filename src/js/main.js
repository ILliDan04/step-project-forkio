$("#mainMenuToggle").on("click", () => {
    $("#mainMenu").slideToggle(200);
});

$(window).on("resize", () => {
    if (window.innerWidth >= 481) {
        $("#mainMenu").removeAttr("style");
    }
});